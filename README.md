# docker-webtop

## Why

This image is a base image for a linux with a GUI running either on a local docker or on MyDocker. 
It is based on [WebTop image from LinuxServer.io](https://github.com/linuxserver/docker-webtop) 
which is maintained and quite often updated.

## Changes

- When deployed on MyDocker, http basic authentification is used.

## Usage

- Choose one of the available branch (`xfce` or `kde` or `icewm`). The most up to date is `xfce`.
